from datetime import datetime

from pydantic import BaseModel, AnyHttpUrl, fields


class PackageFileHashes(BaseModel):
    sha256: str


class PackageFile(BaseModel):
    filename: str
    hashes: PackageFileHashes
    url: AnyHttpUrl
    size: int = None
    yanked: bool | str = False
    requires_python: str | None = fields.Field(None, alias='requires-python')
    upload_time: datetime = fields.Field(None, alias='upload-time')


class PackageMeta(BaseModel):
    last_serial: int = fields.Field(..., alias='_last-serial')
    api_version: str = fields.Field(..., alias='api-version')


class Package(BaseModel):
    name: str
    meta: PackageMeta
    files: list[PackageFile]
    versions: list[str]

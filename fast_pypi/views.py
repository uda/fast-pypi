import fastapi

router = fastapi.APIRouter()


@router.get('/{package}')
async def simple_package(package: str):
    return {
        'name': '',
        'meta': {},
        'files': [],
    }

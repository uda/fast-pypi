import aiohttp
from fastapi import HTTPException
from fastapi.responses import ORJSONResponse
from starlette.status import HTTP_404_NOT_FOUND

from .app import app
from .models import Package


class PyPISimpleJSONResponse(ORJSONResponse):
    media_type = 'application/vnd.pypi.simple.v1+json'


@app.get('/simple/{package:str}/', response_class=PyPISimpleJSONResponse, response_model=Package)
async def simple_package(package: str):
    headers = {
        'Accept': 'application/vnd.pypi.simple.v1+json',
    }
    async with aiohttp.ClientSession(headers=headers) as session:
        async with session.get(f'https://pypi.org/simple/{package}') as response:
            if response.status != 200:
                raise HTTPException(status_code=404)
            pypi_package = await response.json()

    package_model = Package(**pypi_package)

    return package_model
